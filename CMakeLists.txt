cmake_minimum_required(VERSION 3.0)

SET(CMAKE_CXX_FLAGS -pthread)
SET(CMAKE_CXX_STANDARD 11)

# set the project name
project(word_encorer)

find_package(Qt5 COMPONENTS Core REQUIRED)

include_directories(${PROJECT_SOURCE_DIR}/3rdparty)

# add the executable
add_executable(word_encorer src/main.cpp
		src/word_coder.cpp
		src/string_boost.cpp
	)

target_link_libraries(word_encorer Qt5::Core)