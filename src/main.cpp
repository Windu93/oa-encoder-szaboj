#include "word_coder.h"

bool get_files(string & input, string & output)
{
    cout << "Add input file" << endl;
    cin >> input;

    if(!QFileInfo(QString::fromStdString(input)).exists())
    {
        cout << "INVALID INPUT FILE" << endl;
        return false;
    }

    cout << "Add output file" << endl;
    cin >> output;

    return true;
}

int main() {
    
    WordCoder wordCoder;
    string path;

    do
    {   
        cout << "Add path to file" << endl;
        cin >> path;
        
    } while (!wordCoder.load_dictionary(path));  
    
    
    bool quit = false;

    while (!quit)
    {
        string command;
        cout << "Add one of the commands: ENCODE, DECODE, ENCODE_FILE, DECODE_FILE, EXIT" << endl;
        cin >> command;

        if (command == "ENCODE")
        {
            string word;
            cout << "Add the word to encode" << endl;
            cin >> word;
            cout << wordCoder.encode(word) << endl;

        }
        else if (command == "DECODE")
        {
            string word;
            cout << "Add the word to decode" << endl;
            cin >> word;
            cout << wordCoder.decode(word) << endl;

        }
        else if (command == "ENCODE_FILE")
        {
            string input, output;
            if (get_files(input, output))
                wordCoder.encode_file(input, output);

        }
        else if (command == "DECODE_FILE")
        {
            string input, output;
            if (get_files(input, output))
                wordCoder.decode_file(input, output);

        }

        else if (command == "EXIT")
            quit = true;
        else
            cout << "Invalid command" << endl;
        
    }

    return 0;
}
