#include "string_boost.h"

bool compare_no_case(const string & str1, const string & str2, const int from, const int to)
{
    for (int i = from, j = 0; i < to && i < str1.length() && j < str2.length(); i++, j++)
    {
        if (std::tolower(str1[i]) != std::tolower(str2[j]))
            return false;
    }

    return true;
            
}