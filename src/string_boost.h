#pragma once

#include <string>

using std::string;

bool compare_no_case(const string & str1, const string & str2, const int from, const int to);