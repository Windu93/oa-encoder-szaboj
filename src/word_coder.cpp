#include "word_coder.h"

WordCoder::WordCoder()
{
}

WordCoder::~WordCoder()
{
}

bool WordCoder::load_dictionary(const string & path)
{
    if(!QFileInfo(QString::fromStdString(path)).exists())
    {
        cout << "INVALID INPUT FILE" << endl;
        return false;
    }

    ifstream json_file(path);

    try
    {
        json_file >> dictionary;
    }
    catch(...)
    {
        json_file.close();
        cout << "Json file cannot be parsed" << endl;
        return false;
    }
    
    json_file.close();
    return true;

}

string WordCoder::encode(const string & input)
{
    string encoded("");

    for (const auto & c : input)
    {
        string key(1, std::tolower(c));

        try
        {
            encoded += dictionary[key];
        }
        catch(...)
        {
            cout << "INVALID INPUT" << endl;
            break;
            return "";
        }   
        
    }

    return encoded;
}

string WordCoder::decode(const string & input)
{
    string decoded("");
        int actual_pos = 0;
        

        bool decodable;

        do
        {
            decodable = false;
            for (const auto & i : dictionary.items())
            {
                if (compare_no_case(input, string(i.value()), actual_pos, string(i.value()).length()))
                //if (input.compare(actual_pos, string(i.value()).length(), string(i.value())) == 0)
                {
                    decoded += i.key();
                    actual_pos += string(i.value()).length();
                    decodable = true;
                    break;
                }
            }
        }
        while (actual_pos < input.length() && decodable); 

        if (!decodable)
        {
            cout << "INVALID INPUT" << endl;
            return "";
        }

        return decoded;
}

void WordCoder::code_file(const string & input, const string & output, bool encodeMode)
{
    ifstream in_file(input);
    std::ofstream out_file(output);

    while (!in_file.eof())
    {
        string line;
        std::getline(in_file, line);
        out_file << (encodeMode ? encode(line) : decode(line)) << endl;
    }

    in_file.close();
    out_file.close();
}

void WordCoder::encode_file(const string & input, const string & output)
{
    code_file(input, output, true);
}

void WordCoder::decode_file(const string & input, const string & output)
{
    code_file(input, output, false);
}
