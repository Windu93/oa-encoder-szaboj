#pragma once

#include <string>
#include <iostream>
#include <fstream>
#include <QFile>
#include <QFileInfo>
#include <json.h>

#include "string_boost.h"

using namespace nlohmann;

using std::cout;
using std::cin;
using std::cerr;
using std::endl;
using std::string;
using std::ifstream;

class WordCoder
{
private:
    json dictionary;
    void code_file(const string & input, const string & output, bool encodeMode);
public:
    WordCoder();
    ~WordCoder();
    bool load_dictionary(const string & path);
    string encode(const string & input);
    string decode(const string & input);
    void encode_file(const string & input, const string & output);
    void decode_file(const string & input, const string & output);
};
